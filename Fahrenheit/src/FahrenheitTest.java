import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {

	public FahrenheitTest() {};
	
	@Test //TEST ONE - SUCCESS
	public void testConvertFromCelsius() {
		int fahrenheit = Fahrenheit.convertFromCelsius(100);
		System.out.println(fahrenheit);
		assertTrue("Fahrenheit was NOT calculated properly.", 
				fahrenheit == 212);
	}
	
	@Test //TEST TWO - FAILURE
	public void testConvertFromCelsiusException() {
		int fahrenheit = Fahrenheit.convertFromCelsius(10);
		System.out.println(fahrenheit);
		assertTrue("Fahrenheit was NOT calculated properly.", 
				fahrenheit == 50);
	}	
	
	@Test //TEST THREE - FAILURE <- SUCCEEDS BARELY
	public void testConvertFromCelsiusBoundaryIn() {
		int fahrenheit = Fahrenheit.convertFromCelsius(6);
		Math.ceil(fahrenheit);
		System.out.println(fahrenheit);
		assertTrue("Fahrenheit was NOT calculated properly.", 
				fahrenheit == 42);
	}	

	@Test //TEST FOUR - FAILURE
	public void testConvertFromCelsiusBoundaryOut() {
		int fahrenheit = (int) Math.ceil(Fahrenheit.convertFromCelsius(4));
		System.out.println(fahrenheit);
		assertTrue("Fahrenheit was NOT calculated properly.", 
				fahrenheit == 39);
	}		
}
